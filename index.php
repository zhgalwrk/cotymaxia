<!DOCTYPE html>
<html lang="pl">
<!-- Maximizer -->
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zostawiam Ci link do strony) Sprawdź to sam!</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/videoOverlay.css">
    <link rel="stylesheet" href="./css/layout_50kweek.css">
    <link rel="stylesheet" href="./css/layout_country_pl.css">
    <link rel="icon" href="./img/50k_favicon_euro.png">
    <link rel="stylesheet" href="./css/intlTelInput.css">
    <link rel="stylesheet" href="./css/style.css">
    <?=$neogara?>
    <?=$metrika?>
</head>


<body>
    <div class="modalWindow preloader">
        <div class="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="modalBg"></div>
    </div>

    <div id="app">
       
        <header>
            <div class="container">

                <h1 style="display: flex;justify-content: center;align-items: center;">
                    <img class="img-responsive center-block hidden-xs hidden-sm hidden-md" src="./img/pl_flag.png">
                    <span class="jhtxt" style="font-size: 16px">
                        <span class="orangeText" style="font-size: 33.5px">INNOWACYJNA TECHNOLOGIAJUŻ W POLSCE</span>
                        </br>
                        ZOSTAŃ JEDNYM Z PIERWSZYCH 1000 UCZESTNIKÓW I ZARABIAJ OD 19 000 ZŁ TYGODNIOWO</span>

                    <img class="img-responsive center-block" src="./img/pl_flag.png">
                </h1>

            </div>
        </header>

        <main>
            <div class="disclaimerHeader">
                - Sponsorowany -
            </div>
            <div class="container">
                <div class="videoWrapBg">
                    <div class="videoWrapBorder">
                        <div class="videoWrap">
                            <div class="videoOverlayInner" id="videoOverlayInner">
                                <div style="padding:56.25% 0 0 0;position:relative;">
                                    <iframe src="./local_video.html" frameborder="0"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="formOuterWrap">
                    <div class="formInnerWrap">

                        <form class="neo_form" action="send.php" method="post">
                            <?=$hiddens?>
                            <div class="col-xs-12">
                                <input type="text" class="js-name" id="name" required placeholder="Wpisz swoje imię" name="firstname" />
                            </div>
                            <div class="col-xs-12">
                                <input type="text" class="js-lastname" id="lastname" required placeholder="Wpisz swoje nazwisko" name="lastname" />
                            </div>
                            <div class="col-xs-12">
                                <input type="email" class="js-email" id="email" required placeholder="Wpisz swój adres e-mail" name="email" />
                            </div>
                            <div class="col-xs-12">
                                <input type="tel" class="js-phone" id="phone" required name="phone_number" />
                            </div>
                            <div class="col-xs-12 offer_row">
                                <input type="checkbox" name="oferta" checked="" class="js-oferta" />
                                <span>Wyrażam zgodę na przetwarzanie danych osobowych i otrzymywanie materiałów reklamowych oraz zgadzam się z <a href="oferta.php" class="f_lnk" target="_blank">ofertą publiczną</a></span>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="subscribeBtn" name="submitBtn">Rozpocznij TERAZ</button>
                            </div>
                        </form>
                        Nie musisz mieć karty kredytowej, by używać swojego darmowego systemu
                    </div>
                </div>
                <div class="videoFooter">
                    <h2>Nowe miejsca już dostępne dla otwartej rejestracji:
                        <spot-counter></spot-counter>
                    </h2>
                </div>
            </div>
        </main>
        <footer>
            <div class="container">
                &copy; <?=date("Y")?> <?print_r($_SERVER['HTTP_HOST']);?>
             </div>
            <div class="container">
                <div class="disclaimerFooter" id="disclaimertext">
                    
                </div>
            </div>
        </footer>
    </div>
    <style>
        .formWrapper {
            max-width: 430px;
        }

        .neo_form {
            max-width: 430px;
        }

        .neo_form .offer_row {
            /*padding: 0;*/
            text-align: left;
            position: relative;
            top: -10px;
        }

        .neo_form .offer_row input {
            display: inline-block;
            width: auto;
            margin: 0;
            margin-right: 5px;
            float: left;
        }

        input.js-oferta {
            width: auto !important;
            height: auto !important;
        }

        .neo_form .offer_row span {
            font-size: 11px;
            line-height: 14px;
            font-family: sans-serif;
            color: #fff;
            position: relative;
            top: -1px;
            display: block;
        }

        .neo_form .offer_row a {
            color: #fff;
        }
    </style>
    <script src="./js/app.js"></script>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/js.cookie.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>

    <script src="./js/intlTelInput-jquery.min.js"></script>
    <script src="./js/jquery.mask.min.js"></script>
    <script src="./js/main.js"></script>
    <?=$partner?>
</body>

</html>