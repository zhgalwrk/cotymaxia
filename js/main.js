(function($) {
    $(document).ready(function() {
        $("input.js-phone").keyup(function() {
            var val = $(this).val();
            regVal = /[^\d\+\(\)\-]/g;
            newval = val.replace(regVal, "");
            $(this).val(newval);
        });

        $(".js-phone").intlTelInput({
            geoIpLookup: function(callback) {
                var geo = '';
                if (geo == '') {
                    $.get("//ipinfo.io", function() {}, "jsonp").always(function(resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                } else {
                    callback(geo);
                }
            },
            initialCountry: "auto",
            nationalMode: false,
            utilsScript: "./js/utils.js",
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                return selectedCountryPlaceholder;
            },
        });
    });
})(jQuery);

function checkIp() {
    $.getJSON('https://apileads.3snet.tech/check-ip', function(data) {
        console.log(data);
        if (typeof data.ip != 'undefined') {
            var ip = data.ip;
            $('input[name=ip]').attr('value', ip);
        }
    });
};