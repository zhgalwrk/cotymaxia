<?php if (file_exists("functions.php")) { require_once("functions.php"); }?>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Success</title>
    <link rel="stylesheet" href="./css/style.css">
    <?=$metrika?>
</head>

<body class="successPage">
<?=$pixel_img?>
    <div class="container">
        <h1>Dziękujemy za rejestrację!</h1>
        <p>Aby przetworzyć aplikację, musisz potwierdzić swoją wiadomość e-mail.</p>
        <p>Proszę, sprawdź <a href="/" target="_blank" class="mailUrl">swoją skrzynkę odbiorczą</a></p>
    </div>

    <script src="./js/jquery-latest.min.js"></script>
    <script src="./js/intlTelInput-jquery.min.js"></script>
    <script src="./js/main.js"></script>
</body>

</html>